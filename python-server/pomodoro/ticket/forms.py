 # -*- coding: utf-8 -*-
from django.forms import ModelForm
from ticket.models import Action
from ticket.models import Ticket
from django.forms.widgets import SelectMultiple


class BootstrapForm(ModelForm):

	def __init__(self,*args,**kwargs):
		super(ModelForm, self).__init__(*args, **kwargs)
		for field in self:
			print vars(field.field.widget),type(field),type(field.field),type(field.field.widget)
			field.field.widget.attrs["class"] = "form-control"
			if isinstance(field.field.widget,SelectMultiple):
				field.field.widget.attrs["multiselect"] = "showall"
			else:
				field.field.widget.attrs["placeholder"] = field.field.help_text


class ActionForm(BootstrapForm):
	class Meta:
		model = Action
		fields = ["title","description","ticket"]

class TicketForm(BootstrapForm):
	class Meta:
		model = Ticket
		fields = ["title","description","parent","categories"]