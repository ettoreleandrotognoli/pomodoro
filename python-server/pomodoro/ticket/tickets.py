 # -*- coding: utf-8 -*-
from django.shortcuts import render,redirect,get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages


from ticket.models import Ticket
from ticket.models import TicketState
from ticket.models import Action
from ticket.models import Author

from ticket.forms import ActionForm
from ticket.forms import TicketForm

@login_required
@permission_required('ticket.list_ticket',raise_exception=True)
def find(request):
	tickets = Ticket.objects.all().order_by('id')
	ticketCategory = request.GET.getlist('ticketCategory',False)
	if ticketCategory :
		tickets = tickets.filter(categories__id__in=ticketCategory)
	actionCategory = request.GET.getlist('actionCategory',False)
	if actionCategory :
		tickets = tickets.filter(actions__category__id__in=actionCategory)
	author = request.GET.getlist('author',False)
	if author:
		tickets = tickets.filter(actions__author__id__in=author)
	offset = int(request.GET.get('offset',0))
	limit = request.GET.get('limit',None)
	if limit :
		limit = int(limit) + offset
	return render(request,"ticket/find.html",{"tickets":tickets[offset:limit]})

def show(request,ticketId):

	ticket = get_object_or_404(Ticket,pk=int(ticketId))
	if request.method == 'POST':
		actionForm = ActionForm(request.POST,prefix="add-action")
		try:
			action = actionForm.save(commit=False)
			action.author = Author.objects.get(id=request.user.id)
			action.save()
			print action
			return redirect(reverse("ticket:show-ticket",kwargs={"ticketId":ticket.id}))
		except Exception,e:
			print e
	elif request.method == 'GET':
		actionForm = ActionForm(prefix="add-action")

	return render(request,"ticket/detail.html",{"ticket":ticket,"actionForm":actionForm})

@permission_required('ticket.add_ticket',raise_exception=True)
def new(request):
	form = TicketForm(request.POST)
	ticket = form.save(commit=False)
	ticket.state = TicketState.objects.get(id=1)
	ticket.save()
	form = TicketForm(request.POST,instance=ticket)
	form.save()
	messages.success(request,"Ticket Adicionado com Sucesso!")
	return redirect(reverse("ticket:show-ticket",kwargs={"ticketId":ticket.id}))


@permission_required('ticket.add_ticket',raise_exception=True)
def addChild(request,ticketId):
	ticket = get_object_or_404(Ticket,pk=int(ticketId))
	form = TicketForm()

	print (ticket.categories.all)
	return render(request,"ticket/new.html",{"parent":ticket,"form":form})

@permission_required('ticket.add_action',raise_exception=True)
def addAction(request,ticketId):
	ticket = get_object_or_404(Ticket,pk=int(ticketId))
