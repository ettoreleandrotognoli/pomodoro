 # -*- coding: utf-8 -*-
from django.shortcuts import render,redirect,get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages


from ticket.models import Ticket
from ticket.models import TicketState
from ticket.models import Action
from ticket.models import Author

from ticket.forms import ActionForm
from ticket.forms import TicketForm

# Create your views here.
def ping(request):
	if request.method == 'GET':
		form = ActionForm()
	elif request.method == 'POST':
		form = ActionForm(request.POST)
	print form
	for field in form:
		print vars(field)
		print vars(field.field)
		print vars(field.field.widget)
		print "\n"
	return render(request,"form.html",{"form":form})


def newAction(request):
	ticket = get_object_or_404(Ticket,pk=request.POST.get("ticket",-1))
	form = ActionForm(request.POST)
	try:
		action = form.save(commit=False)
	except Exception as e:
		#messages.error(request,str(e))
		return render(request,"action/new.html",{"form":form,"ticket":ticket})
		#return redirect(reverse("ticket:show-ticket",kwargs={"ticketId":1}))
	action.author = Author.objects.get(id=request.user.id)
	action.save()
	messages.success(request,"Iteracao Efetuada com Sucesso!");
	return showAction(request,action.id)


def showAction(request,actionId):
	action = get_object_or_404(Action,pk=int(actionId))
	ticketId = action.ticket.id
	url = reverse("ticket:show-ticket",kwargs={"ticketId":ticketId})
	url += "#action-%d" % action.id
	return HttpResponseRedirect(url)

@login_required
def showAuthor(request,authorName):
	author = get_object_or_404(Author,username=authorName)
	return render(request,"author/detail.html",{"author":author})

@login_required
def home(request):
	author = get_object_or_404(Author,pk=request.user.id)
	return render(request,"author/home.html",{"author":author})