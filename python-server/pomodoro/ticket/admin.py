from django.contrib import admin
from django.contrib.auth.models import Permission

from models import Ticket
from models import TicketFlow
from models import TicketCategory
from models import State
from models import TicketState
from models import Action
from models import ActionCategory

# Register your models here.

@admin.register(Permission)
class PermissionAdmin(admin.ModelAdmin):
	pass

@admin.register(TicketCategory)
class TicketCategoryAdmin(admin.ModelAdmin):
	pass

@admin.register(State)
class StateAdmin(admin.ModelAdmin):
	pass

@admin.register(TicketFlow)
class TicketFlowAdmin(admin.ModelAdmin):
	pass

@admin.register(TicketState)
class TicketStateAdmin(admin.ModelAdmin):
	pass	

@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
	search_fields = ("id","title","description","actions__title","actions__description")
	list_filter = ("categories","actions__category","actions__author")

@admin.register(Action)
class ActionAdmin(admin.ModelAdmin):
	search_fields = ("id","title","description","author__username")
	list_filter = ("category",)

@admin.register(ActionCategory)
class ActionCategoryAdmin(admin.ModelAdmin):
	pass