from django.conf.urls import include, url
from ticket import views
from ticket import tickets

urlpatterns = [
	url(r"^ping/",views.ping,name="ping-pong"),
	url(r"^home/",views.home,name="home"),

	url(r"^$",tickets.find,name="find-ticket"),
	url(r"^(?P<ticketId>[0-9]+)/$",tickets.show,name="show-ticket"),
	url(r"^(?P<ticketId>[0-9]+)/child/$",tickets.addChild,name="add-child-ticket"),
	url(r"^(?P<ticketId>[0-9]+)/interact/$",tickets.addAction,name="add-action-ticket"),
	url(r"^new/$",tickets.new,name="new-ticket"),

	url(r"^action/(?P<actionId>[0-9]+)/$",views.showAction,name="show-action"),
	url(r"^action/new/$",views.newAction,name="new-action"),
	url(r"^author/(?P<authorName>[a-zA-Z0-9]+)/$",views.showAuthor,name="show-author"),
]
