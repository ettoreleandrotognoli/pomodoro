import datetime
from django.utils.encoding import python_2_unicode_compatible
from django.db.models.signals import pre_save,post_save
from django.db import models
from django.contrib import auth
from django.dispatch import receiver

# Create your models here.

class Author(auth.models.User):

	class Meta:
		proxy = True

	def lastActions(self,limit=30):
		return Action.objects.filter(author=self).order_by("-time")[0:limit]

	def lastTickets(self,limit=10):
		return Ticket.objects.filter(actions__author=self).distinct().order_by("-actions__time")[0:limit]

	def name(self):
		fullName = self.get_full_name()
		if fullName :
			return fullName
		return self.username

@python_2_unicode_compatible
class TicketCategory(models.Model):
	title = models.CharField(max_length=128,blank=False,null=False,help_text="Esse eh um texto de ajuda")
	description = models.TextField(blank=True)
	color = models.CharField(max_length=6,blank=False,default="777777")

	def __str__(self):
		return self.title

@python_2_unicode_compatible
class TicketFlow(models.Model):
	title = models.CharField(max_length=128,blank=False,null=False)
	description = models.TextField(blank=True)

	def __str__(self):
		return self.title

@python_2_unicode_compatible
class State(models.Model):
	title = models.CharField(max_length=128,blank=False,null=False)
	description = models.TextField(blank=True)
	after = models.ManyToManyField("self",null=True,blank=True,symmetrical=False,related_name="before")
	color = models.CharField(max_length=6,blank=False,default="777777")
	flow = models.ForeignKey(TicketFlow,null=False,blank=False)

	def __str__(self):
		return self.title

@python_2_unicode_compatible
class ActionCategory(models.Model):
	title = models.CharField(max_length=128,blank=False,null=False);
	description = models.TextField(blank=True)
	color = models.CharField(max_length=6,blank=False,default="777777")

	def __str__(self):
		return self.title

@python_2_unicode_compatible
class Ticket(models.Model):
	parent = models.ForeignKey("self",related_name="children",null=True,blank=True)
	related = models.ManyToManyField("self",null=True,blank=True)
	title = models.CharField(max_length=128)
	description = models.TextField(blank=True)
	categories = models.ManyToManyField(TicketCategory,null=False)
	flow = models.ForeignKey(TicketFlow,null=False,blank=False)

	def orderedActions(self):
		return Action.objects.filter(ticket=self).order_by("-time")

	def authors(self):
		return Author.objects.distinct().filter(actions__ticket=self)

	def firstAction(self):
		firstAction = Action.objects.filter(ticket=self).order_by("time")[0:1]
		if len(firstAction) == 1:
			return firstAction[0]
		return None

	def lastAction(self):
		lastAction = Action.objects.filter(ticket=self).order_by("-time")[0:1]
		if len(lastAction) == 1:
			return lastAction[0]
		return None

	def state(self):
		lastStates = TicketState.objects.filter(ticket=self).order_by("-time")[0:1]
		if len(lastStates) == 1:
			return lastStates[0]
		return None

	def color(self):
		state = self.state()
		if state :
			return state.color()
		return "FF0000"

	def __str__(self):
		return self.title

@python_2_unicode_compatible
class TicketState(models.Model):
	author = models.ForeignKey(Author,null=False,blank=False)
	time = models.DateTimeField(auto_now_add=True,editable=False)
	state = models.ForeignKey(State,null=False,blank=False)
	ticket = models.ForeignKey(Ticket,related_name="states",null=False,blank=False)
	description = models.TextField(blank=True)

	def color(self):
		return self.state.color

	def __str__(self):
		return "%s (%s)" % (self.state.title,self.time)

@python_2_unicode_compatible
class Action(models.Model):
	time = models.DateTimeField(auto_now_add=True,editable=False)
	ticket = models.ForeignKey(Ticket,null=False,related_name="actions")
	title = models.CharField("teste",max_length=128,blank=False,help_text="Esse eh um texto de ajuda")
	description = models.TextField(blank=True,help_text="Esse eh um texto de ajuda")
	author = models.ForeignKey(Author,related_name="actions",null=False)
	category = models.ManyToManyField(ActionCategory,null=True,blank=True)

	def __str__(self):
		return self.title



