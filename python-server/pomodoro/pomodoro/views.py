from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib import auth

# Create your views here.
def home(request):
	return render(request,"admin.html");

def login(request):
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = auth.authenticate(username=username,password=password)
		if user is not None and user.is_active:
			auth.login(request,user)
			return HttpResponseRedirect(request.GET.get('next',reverse("home")))

	return render(request,"login.html")

def logout(request):
	auth.logout(request)
	return render(request,"home.html")

