from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'pomodoro.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

	url(r"^$", "pomodoro.views.home", name="home"),
	url(r"^accounts/login/$","pomodoro.views.login",name="login"),
	url(r"^accounts/logout/$","pomodoro.views.logout",name="logout"),
    url(r"^root/", include(admin.site.urls)),
    url(r"^ticket/", include("ticket.urls",namespace="ticket")),
]
