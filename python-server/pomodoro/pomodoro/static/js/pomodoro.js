function resizeIframe(obj){
	obj.style.height = 0;
	obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
}

$(function(){
  var href = window.location.href.replace(window.location.origin,"").replace(/\?.*/g,'');
  $("nav [href='"+href+"']").parents("li").addClass("active");
 	
  $("[data-toggle='tooltip']").tooltip();

	$("select[multiple][multiselect=showall]").multiselect({
		buttonWidth: "100%",
		buttonText : function(options,select){
			if(options.length === 0)
				return "----";
			var labels = []
			options.each(function(){
				labels.push($(this).html());
			});
			return labels.join(", ");
		}
	});

  $(document).on("click",".alert",function(){
    $(this).fadeOut({complete:function(){$(this).remove();}});
  });

  var successAlerts = $(".alert-success");
  window.setTimeout(function(){successAlerts.fadeOut({complete:function(){$(this).remove()}})},3500);

  $(".nav-tabs[role=tablist]").tab("show");


  $(".ajax-form form,form.ajax-form").ajaxForm({
  	target:null,
  	success : function(response,status,xhr,form){
  		console.debug(response,status,xhr,form);
  		$(form).closest('.ajax-form').after($(response));
  		$(form).closest('.ajax-form').remove();
  	}
  });


});