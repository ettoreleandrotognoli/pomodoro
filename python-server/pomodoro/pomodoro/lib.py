from django.contrib import messages
from django.contrib.messages import get_messages
from ticket.models import Author


def pomodoro(request):
	if not request.user:
		return {}
	try:
		author = Author.objects.get(id=request.user.id)
	except Author.DoesNotExist as error: 
		return {}
	return {"currentAuthor":author}

class PomodoroMiddleware(object):

	def __init__(self):
		pass

	def process_request(self,request):
		#messages.info(request,"teste de mensagem")
		#messages.error(request,"teste de mensagem")
		#messages.success(request,"teste de mensagem")
		#messages.warning(request,"teste de mensagem")
		return None

	def process_view(self,request,view_func,view_args,view_kwargs):
		#print "View KWARGS %s" % str(view_kwargs)
		return None

	def process_response(self,request,response):
		#print "Response: %s" % str(response)
		return response