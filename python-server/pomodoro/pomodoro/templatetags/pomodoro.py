 # -*- coding: utf-8 -*-
from django import template
from django.utils.safestring import mark_safe
from django.utils.safestring import SafeString
from django.utils.html import escape
from django.core.urlresolvers import reverse
import re
from django.template.loader import get_template
from ticket.models import TicketCategory

register = template.Library()

ticketRegex = re.compile("#(?P<ticketId>[0-9]+)")
authorRegex = re.compile("@(?P<authorName>[a-zA-Z0-9]+)")
actionRegex = re.compile("\!(?P<actionId>[0-9]+)")


@register.filter
def prepare(value):
	escapedValue = escape(value)
	ticketUrl = reverse("ticket:show-ticket",kwargs={"ticketId":501}).replace("501","\g<ticketId>")
	authorUrl = reverse("ticket:show-author",kwargs={"authorName":501}).replace("501","\g<authorName>")
	actionUrl = reverse("ticket:show-action",kwargs={"actionId":501}).replace("501","\g<actionId>")
	prepared = ticketRegex.sub(r'<a href="%s" class="label label-default" >#\g<ticketId></a>' % ticketUrl ,escapedValue)
	prepared = authorRegex.sub(r'<a href="%s" class="label label-default" >@\g<authorName></a>' % authorUrl,prepared)
	prepared = actionRegex.sub(r'<a href="%s" class="label label-default">!\g<actionId></a>' % actionUrl,prepared)
	return mark_safe(prepared)

@register.filter
def msgClass(tags):
	tags = escape(tags)
	if tags == "error":
		return mark_safe("danger")
	return mark_safe(tags)


@register.assignment_tag
def allTicketCategory():
	return TicketCategory.objects.all().order_by("title")
