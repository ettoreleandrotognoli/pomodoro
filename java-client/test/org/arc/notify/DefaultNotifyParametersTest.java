/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify;

import org.arc.notify.DefaultNotifyParameters;
import java.awt.Toolkit;
import java.util.Arrays;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ettore
 */
public class DefaultNotifyParametersTest {
    
    public DefaultNotifyParametersTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void jsonConstructWithSummaryAndBody() throws Exception {
        String jsonInit = "{ summary:\"Teste\",body:\"Teste\" }";
        DefaultNotifyParameters parameters = new DefaultNotifyParameters(new JSONObject(jsonInit));
        Assert.assertEquals("Teste", parameters.getSummary());
        Assert.assertEquals("Teste", parameters.getBody());
        Assert.assertEquals(null, parameters.getIcon());
        Assert.assertEquals(null, parameters.getTime());
        Assert.assertArrayEquals(new Object[]{}, parameters.getOptions());
    }
    
    @Test
    public void jsonConstructWithSummaryBodyAndIcon() throws Exception {
        String jsonInit = "{ summary:\"Teste\",body:\"Teste\",icon:\"pomodoro.png\" }";
        DefaultNotifyParameters parameters = new DefaultNotifyParameters(new JSONObject(jsonInit));
        Assert.assertEquals("Teste", parameters.getSummary());
        Assert.assertEquals("Teste", parameters.getBody());
        Assert.assertEquals(Toolkit.getDefaultToolkit().getImage("pomodoro.png"), parameters.getIcon());
        Assert.assertEquals(null, parameters.getTime());
        Assert.assertArrayEquals(new Object[]{}, parameters.getOptions());
    }
    
    @Test
    public void jsonConstructWithSummaryBodyIconAndTime() throws Exception {
        String jsonInit = "{ summary:\"Teste\",body:\"Teste\",icon:\"pomodoro.png\",time=10000 }";
        DefaultNotifyParameters parameters = new DefaultNotifyParameters(new JSONObject(jsonInit));
        Assert.assertEquals("Teste", parameters.getSummary());
        Assert.assertEquals("Teste", parameters.getBody());
        Assert.assertEquals(Toolkit.getDefaultToolkit().getImage("pomodoro.png"), parameters.getIcon());
        Assert.assertEquals(new Long(10000l), parameters.getTime());
        Assert.assertArrayEquals(new Object[]{}, parameters.getOptions());
    }
    
    @Test
    public void jsonConstructWithAllAndOptionStringArray() throws Exception {
        String jsonInit = "{ summary:\"Teste\",body:\"Teste\",icon:\"pomodoro.png\",time:10000,options:[\"Yes\",\"No\"]}";
        DefaultNotifyParameters parameters = new DefaultNotifyParameters(new JSONObject(jsonInit));
        Assert.assertEquals("Teste", parameters.getSummary());
        Assert.assertEquals("Teste", parameters.getBody());
        Assert.assertEquals(Toolkit.getDefaultToolkit().getImage("pomodoro.png"), parameters.getIcon());
        Assert.assertEquals(new Long(10000l), parameters.getTime());
        Object[] options = new Object[]{
            new DefaultNotifyParameters.Option("Yes", 0),
            new DefaultNotifyParameters.Option("No", 1)
        };
        Assert.assertArrayEquals(options, parameters.getOptions());
    }
    
    @Test
    public void jsonConstructWithAllAndOptionObjectArray() throws Exception {
        String jsonInit = "{ summary:\"Summary\",body:\"Body\",icon:\"pomodoro.png\",time:10000,options:[{name:\"Sim\"},{name:\"Não\",value:NULL}]}";
        DefaultNotifyParameters parameters = new DefaultNotifyParameters(new JSONObject(jsonInit));
        Assert.assertEquals("Summary", parameters.getSummary());
        Assert.assertEquals("Body", parameters.getBody());
        Assert.assertEquals(Toolkit.getDefaultToolkit().getImage("pomodoro.png"), parameters.getIcon());
        Assert.assertEquals(new Long(10000l), parameters.getTime());
        Object[] options = new Object[]{
            new DefaultNotifyParameters.Option("Sim", 0),
            new DefaultNotifyParameters.Option("Não", null)
        };
        //Assert.assertArrayEquals(options,parameters.getOptions());
        Assert.assertEquals(Arrays.toString(options), Arrays.toString(parameters.getOptions()));
    }
    
    @Test
    public void consoleConstructWithBodyAndSummary() {
        String line = "summary body";
        DefaultNotifyParameters parameters = new DefaultNotifyParameters(line);
        Assert.assertEquals("summary", parameters.getSummary());
        Assert.assertEquals("body", parameters.getBody());
        Assert.assertEquals(null, parameters.getIcon());
        Assert.assertEquals(null, parameters.getTime());
        Assert.assertArrayEquals(new Object[]{}, parameters.getOptions());
    }
    
    @Test
    public void consoleConstructWithBodySummaryAndIcon() {
        String line = "summary body -i pomodoro.png";
        DefaultNotifyParameters parameters = new DefaultNotifyParameters(line);
        Assert.assertEquals("summary", parameters.getSummary());
        Assert.assertEquals("body", parameters.getBody());
        Assert.assertEquals(Toolkit.getDefaultToolkit().getImage("pomodoro.png"), parameters.getIcon());
        Assert.assertEquals(null, parameters.getTime());
        Assert.assertArrayEquals(new Object[]{}, parameters.getOptions());
    }
    @Test
    public void consoleConstructWithBodySummaryAndTime() {
        String line = "summary body -t 5000";
        DefaultNotifyParameters parameters = new DefaultNotifyParameters(line);
        Assert.assertEquals("summary", parameters.getSummary());
        Assert.assertEquals("body", parameters.getBody());
        Assert.assertEquals(null, parameters.getIcon());
        Assert.assertEquals(new Long(5000l), parameters.getTime());
        Assert.assertArrayEquals(new Object[]{}, parameters.getOptions());
    }
    

    // TODO add test methods here.
    // The methods must be annotated With annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
