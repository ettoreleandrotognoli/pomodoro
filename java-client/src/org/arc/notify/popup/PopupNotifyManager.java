/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify.popup;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import javax.swing.JWindow;
import javax.swing.UIManager;
import org.arc.notify.DefaultNotifyParameters;
import org.arc.notify.NotifyParameters;
import org.arc.notify.NotifyResponse;
import org.arc.notify.NotifySender;
import org.arc.notify.panel.DefaultNotifyPanelBuilder;
import org.arc.notify.panel.NotifyPanel;
import org.arc.notify.panel.NotifyPanelBuilder;

/**
 *
 * @author ettore
 */
public class PopupNotifyManager implements NotifySender {
    private static final long DEFAULT_TIME = 3000l;
    private final Timer timer = new Timer();
    private LocationStrategy locationStrategy;
    private NotifyPanelBuilder notifyPanelBuilder;

    private List<JWindow> windows = new LinkedList<>();

    private final WindowListener windowListener = new WindowAdapter() {

        @Override
        public void windowClosed(WindowEvent e) {
            boolean remove = PopupNotifyManager.this.windows.remove(e.getWindow());
            if (remove) {
                PopupNotifyManager.this.refreshPositions();
            }
        }

    };

    private NotifyPopup makeWindow(NotifyPanel panel, boolean onlyInfo) {
        NotifyPopup window = new NotifyPopup(panel, onlyInfo);
        window.addWindowListener(windowListener);
        window.setLayout(new BorderLayout());
        window.add(panel, BorderLayout.CENTER);
        windows.add(window);
        window.pack();
        window.setLocation(locationStrategy.nextLocation(window));
        return window;
    }

    private void refreshPositions() {
        this.locationStrategy.refreshLocation(this.windows);
    }

    public PopupNotifyManager() {
        this(new DefaultLocationStrategy(), new DefaultNotifyPanelBuilder());
    }

    public PopupNotifyManager(LocationStrategy locationStrategy) {
        this(locationStrategy, new DefaultNotifyPanelBuilder());
    }

    public PopupNotifyManager(NotifyPanelBuilder notifyPanelBuilder) {
        this(new DefaultLocationStrategy(), notifyPanelBuilder);
    }

    public PopupNotifyManager(LocationStrategy locationStrategy, NotifyPanelBuilder notifyPanelBuilder) {
        this.locationStrategy = locationStrategy;
        this.notifyPanelBuilder = notifyPanelBuilder;
    }

    public LocationStrategy getLocationStrategy() {
        return locationStrategy;
    }

    public void setLocationStrategy(LocationStrategy locationStrategy) {
        this.locationStrategy = locationStrategy;
    }

    public NotifyPanelBuilder getNotifyPanelBuilder() {
        return notifyPanelBuilder;
    }

    public void setNotifyPanelBuilder(NotifyPanelBuilder notifyPanelBuilder) {
        this.notifyPanelBuilder = notifyPanelBuilder;
    }

    @Override
    public synchronized NotifyResponse notifySend(NotifyParameters parameters) {
        notifyPanelBuilder.setParamertes(parameters);
        final NotifyPanel notifyPanel = notifyPanelBuilder.build();
        NotifyResponse future = notifyPanel.getNotifyResponse();
        final NotifyPopup window = makeWindow(notifyPanel, parameters.getOptions().length == 0);
        window.setVisible(true);
        long time = parameters.getTime() == null ? DEFAULT_TIME : parameters.getTime();
        if (time > 0) {
            this.timer.schedule(window.makeTimerTask(), time);
        }
        return future;
    }

    public static void main(String... args) throws Exception {
        for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            System.out.println(info.getClassName());
            if (!info.getName().contains("GTK")) {
                continue;
            }
            UIManager.setLookAndFeel(info.getClassName());
        }
        //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        PopupNotifyManager sender = new PopupNotifyManager();
        List<NotifyResponse> responses = new LinkedList<>();
        NotifyParameters parameters = new DefaultNotifyParameters("summary body -i pomodoro.png -t 5000 -o yes -o no");
        for (int i = 0; i < 4; i++) {
            responses.add(sender.notifySend(parameters));
            Thread.sleep(200);
        }
        parameters = new DefaultNotifyParameters("summary body -i pomodoro.png -t 0");
        NotifyResponse nullResponse = sender.notifySend(parameters);
        System.out.println("wait");
        Thread.sleep(5000);
        System.out.println("go");
        System.gc();
        for (NotifyResponse resp : responses) {
            System.out.println(resp.get());
        }
        System.out.println(nullResponse.get());
        System.exit(0);
    }
}
