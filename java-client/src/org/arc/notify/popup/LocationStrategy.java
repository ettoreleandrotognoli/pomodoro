/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify.popup;

import java.awt.Point;
import java.util.List;
import javax.swing.JWindow;

/**
 *
 * @author ettore
 */
public interface LocationStrategy {

    public Point nextLocation(JWindow window);

    public void refreshLocation(List<JWindow> windows);

}
