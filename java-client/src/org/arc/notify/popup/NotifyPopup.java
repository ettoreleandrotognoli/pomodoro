/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify.popup;

import java.util.TimerTask;
import javax.swing.JWindow;
import org.arc.notify.event.ChosenOptionEvent;
import org.arc.notify.event.ClickEvent;
import org.arc.notify.event.NotifyListenerAdapter;
import org.arc.notify.panel.NotifyPanel;

/**
 *
 * @author ettore
 */
public class NotifyPopup extends JWindow {

    private NotifyPanel notifyPanel = null;

    private void init(NotifyPanel notifyPanel, boolean closeOnClick) {
        this.setAlwaysOnTop(true);
        this.notifyPanel = notifyPanel;
        this.notifyPanel.addNotifyListener(new NotifyListenerAdapter() {
            @Override
            public void optionWasSelected(ChosenOptionEvent evt) {
                NotifyPopup.this.close();
            }
        });
        if (!closeOnClick) {
            return;
        }
        this.notifyPanel.addNotifyListener(new NotifyListenerAdapter() {
            @Override
            public void notifyClicked(ClickEvent evt) {
                NotifyPopup.this.close();
            }
        });
    }

    public NotifyPopup(NotifyPanel notifyPanel) {
        init(notifyPanel, false);
    }

    public NotifyPopup(NotifyPanel notifyPanel, boolean closeOnClick) {
        init(notifyPanel, closeOnClick);
    }

    public NotifyPanel getNotifyPanel() {
        return notifyPanel;
    }

    public void setNotifyPanel(NotifyPanel notifyPanel) {
        this.notifyPanel = notifyPanel;
    }

    public TimerTask makeTimerTask() {
        return new TimerTask() {
            @Override
            public void run() {
                NotifyPopup.this.close();
            }
        };
    }

    public void close() {
        dispose();
        notifyPanel.close();
    }

}
