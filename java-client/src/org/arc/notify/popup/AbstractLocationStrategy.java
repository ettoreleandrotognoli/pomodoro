/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify.popup;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.util.List;
import java.util.concurrent.Semaphore;
import javax.swing.JWindow;

/**
 *
 * @author ettore
 */
public abstract class AbstractLocationStrategy implements LocationStrategy {

    private final Semaphore semaphore = new Semaphore(1);
    private boolean acquired = false;
    protected Point startLocation;
    protected Point currentPoint;

    public AbstractLocationStrategy(Point startLocation) {
        this.startLocation = startLocation;
        this.currentPoint = startLocation;
    }

    protected Dimension getScreenSize() {
        return Toolkit.getDefaultToolkit().getScreenSize();
    }

    protected void resetLocation() {
        this.currentPoint = startLocation;
    }

    protected abstract Point calcNextLocation(JWindow window);

    @Override
    public Point nextLocation(JWindow window) {
        if (acquired) {
            return calcNextLocation(window);
        }
        try {
            semaphore.acquire();
            return calcNextLocation(window);
        }
        catch (InterruptedException ex) {
            ex.printStackTrace();
            return calcNextLocation(window);
        }
        finally {
            semaphore.release();
        }
    }

    @Override
    public void refreshLocation(List<JWindow> windows) {
        try {
            semaphore.acquire();
            this.acquired = true;
            this.resetLocation();
            for (JWindow window : windows) {
                Point newLocation = nextLocation(window);
                window.setLocation(newLocation);
            }
        }
        catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        finally {
            this.acquired = false;
            semaphore.release();
        }
    }

}
