/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify.popup;

import java.awt.Dimension;
import java.awt.Point;
import javax.swing.JWindow;

/**
 *
 * @author ettore
 */
public class DefaultLocationStrategy extends AbstractLocationStrategy {

    public DefaultLocationStrategy() {
        super(null);
        Dimension size = super.getScreenSize();
        super.startLocation = new Point(size.width - 10, 10);
        super.currentPoint = super.startLocation;
    }

    @Override
    protected Point calcNextLocation(JWindow window) {
        Dimension screenSize = super.getScreenSize();
        Dimension windowSize = window.getSize();
        Point newLocation = new Point(super.currentPoint.x - windowSize.width, super.currentPoint.y);
        super.currentPoint = new Point(super.currentPoint.x, super.currentPoint.y + windowSize.height + 10);
        return newLocation;
    }

}
