/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify;

import org.arc.notify.event.ChosenOptionEvent;
import org.arc.notify.event.CloseEvent;

/**
 *
 * @author ettore
 */
public class AbstractNotifyResponse implements NotifyResponse {

    private Object value = null;
    private boolean done = false;
    private CallbackResponse callback = null;

    protected AbstractNotifyResponse() {

    }

    @Override
    public CallbackResponse getCallback() {
        return callback;
    }

    @Override
    public void setCallback(CallbackResponse callback) {
        this.callback = callback;
        if (this.isDone()) {
            this.callback.call(value);
        }
    }

    @Override
    public boolean isDone() {
        return this.done;
    }

    @Override
    public Object get() throws InterruptedException {
        if (this.done) {
            return this.value;
        }
        else {
            synchronized (this) {
                wait();
                return value;
            }
        }
    }

    protected void setValue(Object value) {
        this.value = value;
        this.done = true;
        synchronized (this) {
            notifyAll();
        }
        if (this.callback != null) {
            this.callback.call(value);
        }
    }

    private void optionWasSelected(ChosenOptionEvent evt) {
        this.setValue(evt.getOption().getValue());
    }

    private void notifyClosed(CloseEvent evt) {
        if (this.done) {
            return;
        }
        this.setValue(null);
    }

}
