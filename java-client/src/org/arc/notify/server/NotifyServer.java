/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify.server;

import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.IllegalBlockingModeException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import org.arc.notify.CallbackResponse;
import org.arc.notify.DefaultNotifyParameters;
import org.arc.notify.NotifyParameters;
import org.arc.notify.NotifyResponse;
import org.arc.notify.NotifySender;
import org.arc.notify.popup.PopupNotifyManager;
import org.arc.util.IO;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author ettore
 */
public class NotifyServer {

    private static final String CONFIG = "org/arc/notify/server/config.json";

    private NotifyServerConfig config;
    private NotifySender notifySender = new PopupNotifyManager();
    private ServerSocket socket = null;
    private boolean on = false;

    public NotifyServer(NotifyServerConfig config) {
        this.config = config;
    }

    class CallbackNotify implements CallbackResponse {

        Socket socket;

        public CallbackNotify(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void call(Object response) {
            try {
                PrintStream printStream = new PrintStream(socket.getOutputStream());
                printStream.println(response);
            }
            catch (IOException ex) {
                Logger.getLogger(NotifyServer.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally {
                try {
                    socket.close();
                }
                catch (IOException ex) {
                    Logger.getLogger(NotifyServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private void init() throws Exception {
        if (config.getLookAndFeel() == null) {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        else {
            UIManager.setLookAndFeel(config.getLookAndFeel());
        }
        this.notifySender = (NotifySender) Class.forName(config.getNotifySender()).newInstance();
        this.socket = new ServerSocket(config.getPort());
    }

    public void start() throws Exception {
        this.init();
        this.on = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                NotifyServer.this.run();
            }
        }).start();
    }

    private Socket accept() {
        Socket sock = null;
        while (on) {
            try {
                sock = this.socket.accept();
                if (!checkLocal(sock)) {
                    sock.close();
                    continue;
                }
                return sock;
            }
            catch (SecurityException | IOException | IllegalBlockingModeException ex) {
                Logger.getLogger(NotifyServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return sock;
    }

    private boolean checkLocal(Socket socket) {
        InetAddress address = socket.getInetAddress();
        return address.isLoopbackAddress();
    }

    private void runNotify(Socket socket) {
        try {
            Scanner scanner = new Scanner(socket.getInputStream());
            String line = scanner.nextLine();
            NotifyParameters parameters = new DefaultNotifyParameters(line);
            NotifyResponse response = notifySender.notifySend(parameters);
            response.setCallback(new CallbackNotify(socket));
        }
        catch (IOException ex) {
            Logger.getLogger(NotifyServer.class.getName()).log(Level.SEVERE, null, ex);
            try {
                socket.close();
            }
            catch (IOException ex1) {
                Logger.getLogger(NotifyServer.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    private void run() {
        while (on) {
            Socket socket = this.accept();
            runNotify(socket);
        }
    }

    public static void main(String... args) throws IOException, JSONException, Exception {
        JSONObject jsonConfig = new JSONObject(IO.getResourceAsString(CONFIG));
        String configKey = jsonConfig.getString("config");
        jsonConfig = jsonConfig.getJSONObject(configKey);
        NotifyServer server = new NotifyServer(new DefaultNotifyServerConfig(jsonConfig));
        System.out.println(jsonConfig);
        server.start();
    }
}
