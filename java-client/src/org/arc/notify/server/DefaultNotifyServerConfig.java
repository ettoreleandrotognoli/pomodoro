/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify.server;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author ettore
 */
public class DefaultNotifyServerConfig implements NotifyServerConfig {

    private int port = 9090;
    private String lookAndFeel = null;
    private String notifySender = null;

    public DefaultNotifyServerConfig(JSONObject jsonConfig) throws JSONException {
        if (jsonConfig.has("port")) {
            this.port = jsonConfig.getInt("port");
        }
        this.lookAndFeel = jsonConfig.getString("lookAndFeel");
        this.notifySender = jsonConfig.getString("notifySender");
    }

    @Override
    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public String getLookAndFeel() {
        return lookAndFeel;
    }

    public void setLookAndFeel(String lookAndFeel) {
        this.lookAndFeel = lookAndFeel;
    }

    @Override
    public String getNotifySender() {
        return notifySender;
    }

    public void setNotifySender(String notifySender) {
        this.notifySender = notifySender;
    }

}
