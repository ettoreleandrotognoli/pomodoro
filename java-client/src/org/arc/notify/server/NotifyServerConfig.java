/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify.server;

/**
 *
 * @author ettore
 */
public interface NotifyServerConfig {

    public String getLookAndFeel();

    public int getPort();

    public String getNotifySender();

}
