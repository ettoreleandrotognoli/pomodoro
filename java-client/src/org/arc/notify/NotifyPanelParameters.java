/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify;

import java.awt.Image;

/**
 *
 * @author ettore
 */
public interface NotifyPanelParameters {

    public String getSummary();

    public String getBody();

    public Image getIcon();

    public NotifyParameters.Option[] getOptions();
    
}
