/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify;

import java.awt.Image;

/**
 *
 * @author ettore
 */
public interface NotifyParameters extends NotifyPanelParameters {

    static interface Option {

        public String getName();

        public Object getValue();
    }

    @Override
    public String getSummary();

    @Override
    public String getBody();

    @Override
    public Image getIcon();

    public Long getTime();

    /**
     *
     * @return
     */
    @Override
    public Option[] getOptions();

}
