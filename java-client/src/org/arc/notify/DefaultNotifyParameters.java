/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify;

import java.awt.Image;
import java.awt.Toolkit;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author ettore
 */
public class DefaultNotifyParameters implements NotifyParameters {

    static class Option implements NotifyParameters.Option {
        String name;
        Object value;

        public Option(String name, Object value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Option{" + "name=" + name + ", value=" + value + '}';
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Option other = (Option) obj;
            if (!Objects.equals(this.name, other.name)) {
                return false;
            }
            return Objects.equals(this.value, other.value);
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 43 * hash + Objects.hashCode(this.name);
            hash = 43 * hash + Objects.hashCode(this.value);
            return hash;
        }

    }

    private String summary;
    private String body;
    private Image icon;
    private Long time;
    private DefaultNotifyParameters.Option[] options;

    public DefaultNotifyParameters(String summary, String body, String icon) {
        this.summary = summary;
        this.body = body;
        this.icon = Toolkit.getDefaultToolkit().getImage(icon);
        this.time = null;
        this.options = new Option[]{};
    }

    public DefaultNotifyParameters(String summary, String body, Image icon, Long time, DefaultNotifyParameters.Option[] options) {
        this.summary = summary;
        this.body = body;
        this.icon = icon;
        this.time = time;
        this.options = options;
    }

    public DefaultNotifyParameters(JSONObject json) throws JSONException {
        this.summary = json.getString("summary");
        this.body = json.getString("body");
        this.icon = json.has("icon") ? Toolkit.getDefaultToolkit().getImage(json.getString("icon")) : null;
        this.time = json.has("time") ? json.getLong("time") : null;
        if (!json.has("options")) {
            this.options = new DefaultNotifyParameters.Option[]{};
            return;
        }
        JSONArray arrayOptions = json.getJSONArray("options");
        if (arrayOptions instanceof JSONArray) {
            this.options = new DefaultNotifyParameters.Option[arrayOptions.length()];
            for (int i = 0; i < this.options.length; i++) {
                Object option = arrayOptions.get(i);
                Object value = i;
                String name = null;
                if (option instanceof String) {
                    name = (String) option;
                    this.options[i] = new DefaultNotifyParameters.Option(name, value);
                    continue;
                }
                JSONObject jsonOption = (JSONObject) option;
                name = jsonOption.getString("name");
                value = jsonOption.has("value") ? jsonOption.get("value") : value;
                this.options[i] = new DefaultNotifyParameters.Option(name, value);
            }
        }
    }

    public DefaultNotifyParameters(String line) {
        List<String> options = new LinkedList();
        String[] explode = line.split(" ");
        this.summary = explode[0];
        this.body = explode[1];
        for (int i = 2; i < explode.length; i++) {
            String opt = explode[i];
            switch (opt.toLowerCase()) {
                case "-o":
                    options.add(explode[++i]);
                    break;
                case "-i":
                    this.icon = Toolkit.getDefaultToolkit().getImage(explode[++i]);
                    break;
                case "-t":
                    this.time = Long.parseLong(explode[++i]);
                    break;
            }
        }
        this.options = new DefaultNotifyParameters.Option[options.size()];
        for (int i = 0; i < this.options.length; i++) {
            this.options[i] = new DefaultNotifyParameters.Option(options.get(i), i);
        }
    }

    @Override
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @Override
    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public Image getIcon() {
        return icon;
    }

    public void setIcon(Image icon) {
        this.icon = icon;
    }

    @Override
    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    @Override
    public DefaultNotifyParameters.Option[] getOptions() {
        return options;
    }

    public void setOptions(DefaultNotifyParameters.Option[] options) {
        this.options = options;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultNotifyParameters other = (DefaultNotifyParameters) obj;
        if (!Objects.equals(this.summary, other.summary)) {
            return false;
        }
        if (!Objects.equals(this.body, other.body)) {
            return false;
        }
        if (!Objects.equals(this.icon, other.icon)) {
            return false;
        }
        if (!Objects.equals(this.time, other.time)) {
            return false;
        }
        return Arrays.deepEquals(this.options, other.options);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.summary);
        hash = 79 * hash + Objects.hashCode(this.body);
        hash = 79 * hash + Objects.hashCode(this.icon);
        hash = 79 * hash + Objects.hashCode(this.time);
        hash = 79 * hash + Arrays.deepHashCode(this.options);
        return hash;
    }

}
