/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify;

/**
 *
 * @author ettore
 */
public interface NotifySender {

    public NotifyResponse notifySend(NotifyParameters parameters);

}
