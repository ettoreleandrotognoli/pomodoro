/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify.event;

import org.arc.notify.NotifyParameters;

/**
 *
 * @author ettore
 */
public class ChosenOptionEvent extends NotifyEvent {

    private final NotifyParameters.Option option;

    public ChosenOptionEvent(Object source, NotifyParameters.Option option) {
        super(source);
        this.option = option;
    }

    public NotifyParameters.Option getOption() {
        return this.option;
    }

}
