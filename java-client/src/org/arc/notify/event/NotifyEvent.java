/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify.event;

import java.util.EventObject;
import javax.swing.JPanel;

/**
 *
 * @author ettore
 */
public class NotifyEvent extends EventObject {

    private JPanel notifyPanel;

    public NotifyEvent(Object source) {
        super(source);
    }

    public JPanel getNotifyPanel() {
        return notifyPanel;
    }

}
