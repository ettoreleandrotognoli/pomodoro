/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify.event;

import java.awt.event.MouseEvent;

/**
 *
 * @author ettore
 */
public class ClickEvent extends NotifyEvent {

    private MouseEvent mouseEvent;

    public ClickEvent(Object source, MouseEvent mouseEvent) {
        super(source);
        this.mouseEvent = mouseEvent;
    }

    public MouseEvent getMouseEvent() {
        return mouseEvent;
    }

}
