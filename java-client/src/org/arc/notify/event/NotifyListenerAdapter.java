/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify.event;

/**
 *
 * @author ettore
 */
public abstract class NotifyListenerAdapter implements NotifyListener {

    @Override
    public void optionWasSelected(ChosenOptionEvent evt) {

    }

    @Override
    public void notifyClosed(CloseEvent evt) {

    }

    @Override
    public void notifyClicked(ClickEvent evt) {

    }

}
