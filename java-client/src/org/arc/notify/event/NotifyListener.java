/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify.event;

import java.util.EventListener;

/**
 *
 * @author ettore
 */
public interface NotifyListener extends EventListener {

    public void optionWasSelected(ChosenOptionEvent evt);

    public void notifyClosed(CloseEvent evt);

    public void notifyClicked(ClickEvent evt);

}
