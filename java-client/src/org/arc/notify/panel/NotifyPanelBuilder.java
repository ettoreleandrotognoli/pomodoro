/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify.panel;

import java.awt.Image;
import org.arc.notify.NotifyPanelParameters;
import org.arc.notify.NotifyParameters;

/**
 *
 * @author ettore
 */
public interface NotifyPanelBuilder {

    public void setParamertes(NotifyPanelParameters parameters);

    public void setSummary(String title);

    public void setBody(String body);

    public void setIcon(Image icon);

    public void setOptions(NotifyParameters.Option... options);

    public NotifyPanel build();

}
