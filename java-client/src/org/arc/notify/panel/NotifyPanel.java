/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify.panel;

import java.awt.LayoutManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Objects;
import javax.swing.JPanel;
import javax.swing.event.EventListenerList;
import org.arc.notify.AbstractNotifyResponse;
import org.arc.notify.NotifyParameters;
import org.arc.notify.NotifyResponse;
import org.arc.notify.event.ChosenOptionEvent;
import org.arc.notify.event.ClickEvent;
import org.arc.notify.event.CloseEvent;
import org.arc.notify.event.NotifyListener;

/**
 *
 * @author ettore
 */
public class NotifyPanel extends JPanel {
    
    private transient final EventListenerList listeners = new EventListenerList();
    
    private class NotifyPanelResponse extends AbstractNotifyResponse {
        @Override
        protected void setValue(Object value) {
            super.setValue(value);
        }
    };
    
    private NotifyPanelResponse notifyResponse = new NotifyPanelResponse();
    
    private NotifyParameters.Option selectedOption = null;
    
    private void init() {
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                fireClickEvent(e);
            }
        });
    }
    
    public void addNotifyListener(NotifyListener... listeners) {
        for (NotifyListener listener : listeners) {
            this.listeners.add(NotifyListener.class, listener);
        }
    }
    
    public void removeNotifyListener(NotifyListener listener) {
        this.listeners.remove(NotifyListener.class, listener);
    }
    
    protected void fireChosenOptionEvent(NotifyParameters.Option option) {
        notifyResponse.setValue(option.getValue());
        ChosenOptionEvent evt = new ChosenOptionEvent(this, option);
        for (NotifyListener listener : this.listeners.getListeners(NotifyListener.class)) {
            listener.optionWasSelected(evt);
        }
    }
    
    protected void fireCloseEvent() {
        if (!notifyResponse.isDone()) {
            notifyResponse.setValue(null);
        }
        CloseEvent evt = new CloseEvent(this);
        for (NotifyListener listener : this.listeners.getListeners(NotifyListener.class)) {
            listener.notifyClosed(evt);
        }
    }
    
    protected void fireClickEvent(MouseEvent mouseEvent) {
        ClickEvent evt = new ClickEvent(this, mouseEvent);
        for (NotifyListener listener : this.listeners.getListeners(NotifyListener.class)) {
            listener.notifyClicked(evt);
        }
    }
    
    public NotifyParameters.Option getSelectedOption() {
        return selectedOption;
    }
    
    public void setSelectedOption(NotifyParameters.Option selectedOption) {
        NotifyParameters.Option old = this.selectedOption;
        this.selectedOption = selectedOption;
        if (!Objects.equals(old, selectedOption)) {
            fireChosenOptionEvent(selectedOption);
        }
    }
    
    public NotifyPanel(LayoutManager layout, boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
        this.init();
    }
    
    public NotifyPanel(LayoutManager layout) {
        super(layout);
        this.init();
    }
    
    public NotifyPanel(boolean isDoubleBuffered) {
        super(isDoubleBuffered);
        this.init();
    }
    
    public NotifyPanel() {
        this.init();
    }
    
    public void close() {
        this.setVisible(false);
        this.fireCloseEvent();
    }
    
    public NotifyResponse getNotifyResponse() {
        return notifyResponse;
    }
    
}
