/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.notify.panel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import org.arc.notify.NotifyPanelParameters;
import org.arc.notify.NotifyParameters;
import org.arc.notify.NotifyParameters.Option;

/**
 *
 * @author ettore
 */
public class DefaultNotifyPanelBuilder implements NotifyPanelBuilder {

    private NotifyPanel notifyPanel = null;
    private String summary;
    private String body;
    private Image icon;
    private NotifyParameters.Option[] options;

    @Override
    public void setParamertes(NotifyPanelParameters parameters) {
        this.summary = parameters.getSummary();
        this.body = parameters.getBody();
        this.icon = parameters.getIcon();
        this.options = parameters.getOptions();
    }

    class ActionListenerAdapter implements ActionListener {

        private final NotifyParameters.Option value;
        private final NotifyPanel notifyPanel;

        public ActionListenerAdapter(NotifyParameters.Option optionValue, NotifyPanel notifyPanel) {
            this.value = optionValue;
            this.notifyPanel = notifyPanel;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.notifyPanel.setSelectedOption(value);
        }
    }

    public String getSummary() {
        return summary;
    }

    @Override
    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getBody() {
        return body;
    }

    @Override
    public void setBody(String body) {
        this.body = body;
    }

    public Image getIcon() {
        return icon;
    }

    @Override
    public void setIcon(Image icon) {
        this.icon = icon;
    }

    public NotifyParameters.Option[] getOptions() {
        return options;
    }

    @Override
    public void setOptions(NotifyParameters.Option[] options) {
        this.options = options;
    }

    private JComponent makeImage() {
        JLabel lbImage = new JLabel(new ImageIcon(this.icon.getScaledInstance(64, 64, Image.SCALE_SMOOTH)));
        return lbImage;
    }

    private JComponent makeOptions() {
        JPanel pnOptions = new JPanel(new FlowLayout());
        for (Option opt : this.options) {
            JButton bt = new JButton(opt.getName());
            pnOptions.add(bt);
            bt.addActionListener(new ActionListenerAdapter(opt, this.notifyPanel));
        }
        return pnOptions;
    }

    private JComponent makeMessage() {
        JPanel msgPanel = new JPanel(new BorderLayout());
        JLabel lbSummary = new JLabel(this.summary, JLabel.CENTER);
        JLabel lbBody = new JLabel(this.body, JLabel.CENTER);
        msgPanel.add(lbSummary, BorderLayout.NORTH);
        msgPanel.add(lbBody, BorderLayout.CENTER);
        return msgPanel;
    }

    @Override
    public NotifyPanel build() {
        notifyPanel = new NotifyPanel(new BorderLayout());
        notifyPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        notifyPanel.setPreferredSize(new Dimension(200, 125));
        notifyPanel.add(makeMessage(), BorderLayout.CENTER);
        if(this.icon != null)
            notifyPanel.add(makeImage(), BorderLayout.WEST);
        notifyPanel.add(makeOptions(), BorderLayout.SOUTH);
        NotifyPanel panel = this.notifyPanel;
        this.notifyPanel = null;
        return panel;
    }

}
