package org.arc.pomodoro;

public interface PomodoroListener {

    public void eventPerformed(PomodoroEvent evt);
}
