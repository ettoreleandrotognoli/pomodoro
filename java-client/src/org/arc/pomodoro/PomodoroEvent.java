package org.arc.pomodoro;

import java.util.EventObject;

public class PomodoroEvent extends EventObject {

    enum Type {
        StartWork,
        EndWork,
        StartPause,
        EndPause,
        StartLongPause,
        EndLongPause,
    };

    private Type type;

    public PomodoroEvent(Pomodoro src, Type type) {
        super(src);
        this.type = type;
    }

    public Type getType() {
        return this.type;
    }

    public String toString() {
        return "PomodoroEvent{type:" + this.type + "}";
    }

}
